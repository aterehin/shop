;(function($){
	$(function(){
		function changePrice() {
			var $price = $('.woocommerce-variation-price > .price').html(),
				$container = $('.summary.entry-summary > .price, .summary.entry-summary .summary-content > .price');

			$container.html($price);
		}

		$(document).on('show_variation', '.single_variation_wrap', changePrice);
	});
})(jQuery);