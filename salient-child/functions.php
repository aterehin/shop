<?php

add_image_size('attribute-color', 25, 25, true);

add_action('tgmpa_register', 'salient_child_register_required_plugins');
function salient_child_register_required_plugins()
{
    $plugins = array(
        array(
            'name' => 'WP Mail SMTP от WPForms',
            'slug' => 'wp-mail-smtp',
            'required' => false,
        ),
        array(
            'name' => 'Яндекс.Касса 2.0 для Woocommerce',
            'slug' => 'yandex-money-checkout',
            'required' => false,
        ),
        array(
            'name' => 'WP Term Colors',
            'slug' => 'wp-term-colors',
            'required' => false,
        ),
        array(
            'name' => 'WP Term Images',
            'slug' => 'wp-term-images',
            'required' => false,
        )
    );

    tgmpa($plugins);
}


add_action('wp_enqueue_scripts', 'salient_child_enqueue_style');
function salient_child_enqueue_style()
{
    $nectar_theme_version = nectar_get_theme_version();

    wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css', array('font-awesome'), $nectar_theme_version);

    if (is_rtl()) {
        wp_enqueue_style('salient-rtl', get_template_directory_uri() . '/rtl.css', array(), '1', 'screen');
    }
}

add_action('wp_enqueue_scripts', 'salient_child_enqueue_scripts');
function salient_child_enqueue_scripts()
{
    $nectar_theme_version = nectar_get_theme_version();

    wp_enqueue_script('salient-child-init', get_stylesheet_directory_uri() . '/js/init.js', 'jquery', $nectar_theme_version, true);
}

add_action('after_setup_theme', 'salient_child_remove_zoom_lightbox_theme_support', 99);
function salient_child_remove_zoom_lightbox_theme_support()
{
    remove_theme_support('wc-product-gallery-zoom');
}

add_filter('woocommerce_layered_nav_term_html', 'salient_child_term_template', 10, 4);
function salient_child_term_template($term_html, $term, $link, $count)
{
    $color = get_term_meta($term->term_id, 'color', true);
    $image = get_term_meta($term->term_id, 'image', true);

    if ($color || $image) {
        if ($image) {
            $image_data = wp_get_attachment_image_src($image, 'attribute-color', true);
            $background = 'url(' . esc_url($image_data[0]) . ');';
        } else {
            $background = $color;
        }

        $term_html = '<a rel="nofollow" class="term-link" href="' . esc_url($link) . '"><span class="term-thumb" style="background: ' . esc_attr($background) . '"></span>' . esc_html($term->name) . '</a><span class="count">' . absint($count) . '</span>';
    }

    return $term_html;
}

add_filter('load_textdomain_mofile', 'salient_child_load_plugin_translation_file', 10, 2);
function salient_child_load_plugin_translation_file($mofile, $domain)
{
    $directory = get_stylesheet_directory();
    $locale = get_locale();

    switch ($domain) {
        case "salient":
            return $directory . '/languages/salient/salient-' . $locale . '.mo';
        case "woocommerce":
            return $directory . '/languages/woocommerce/woocommerce-' . $locale . '.mo';
        default:
            return $mofile;
    }
}

add_filter('woocommerce_dropdown_variation_attribute_options_html', 'salient_child_filter_dropdown_option_html', 12, 2);
function salient_child_filter_dropdown_option_html($html, $args)
{
    $show_option_none_text = $args['show_option_none'] ? $args['show_option_none'] : __('Choose an option', 'woocommerce');
    $show_option_none_html = '<option value="">' . esc_html($show_option_none_text) . '</option>';

    $html = str_replace($show_option_none_html, '', $html);

    return $html;
}

add_filter('woocommerce_enable_order_notes_field', '__return_false');
add_filter('woocommerce_checkout_fields', 'salient_child_override_checkout_fields');
function salient_child_override_checkout_fields($fields)
{
    unset($fields['billing']['billing_postcode']);
    unset($fields['billing']['billing_company']);
    unset($fields['billing']['billing_email']);
    unset($fields['order']['order_comments']);

    return $fields;
}

add_filter('woocommerce_cart_shipping_packages', 'salient_child_cart_shipping_packages', 10, 1);
function salient_child_cart_shipping_packages($packages)
{
    $new_packages = array();

    foreach ($packages as $index => $package) {
        $address = WC_Shiptor_Autofill_Addresses::get_city_by_id($package['destination']['kladr_id']);
        $new_packages[$index] = $package;
        $new_packages[$index]['destination']['state'] = $address['state'];
    }

    return $new_packages;
}

add_filter('woocommerce_cart_shipping_method_full_label', 'salient_child_add_0_to_shipping_label', 10, 2);
function salient_child_add_0_to_shipping_label($label, $method)
{
    if (!($method->cost > 0)) {
        $label .= ': ' . wc_price(0);
    }

    return $label;
}
